from bs4 import BeautifulSoup
import json
infile = open("s.xml","r")
contents = infile.read()
soup = BeautifulSoup(contents,'xml')
family = soup.find_all('surname')
given = soup.find_all('given-names')
var="book"
collab=soup.find('collab')
pub_collab={"author" :collab.get_text()}
print(json.dumps(pub_collab))
for i in range(0, len(family)):
    family_given={"family" :family[i].get_text(),
        "given-name" : given[i].get_text()
       }
    print(json.dumps(family_given))
  
pname=soup.find_all('publisher-name')
for p_name in pname:
  pub_name={
      "publisher" : p_name.get_text()  }
  print(json.dumps(pub_name))

publisher=soup.find_all('publisher-loc')
for publishers in publisher:
  pub_loc={
      "location" : publishers.get_text()  }
  print(json.dumps(pub_loc))

source=soup.find_all('source')
for sources in source:
  p_source={
      "title" : sources.get_text()  }
  print(json.dumps(p_source))

title=soup.find_all(attrs={"publication-type" : "book"})
for i in range(len(title)):
  titles={"type" :var}
  print(json.dumps(titles))

year=soup.find_all('year')
for years in year:
  p_year={
      "date" : years.get_text()  }
  print(json.dumps(p_year))