from bs4 import BeautifulSoup
import json
infile=open('book_reference_type.xml','r')
contents= infile.read()
soup=BeautifulSoup(contents,'xml')


def person_group(reference):
  author=[]
  if reference.find('person-group'):
    all_names=reference.find('person-group').find_all('name')
    for names in all_names:
      author_name=surname_given(names)
      author.append(author_name)
      return author

  
 
def surname_given(names):
  family_given={}
  if names.find('surname'):
    family_given['family']=names.find('surname').get_text()
    family_given['given']=names.find('given-names').get_text()
    return family_given
  
  

def ref_title(reference):
  title=[]
  if reference.find('source'):
    source=reference.find('source').text.replace(' ','').replace('\n',' ')   
    title.append(source)
    return title 
 


def ref_collab(reference):
  collab=[]
  if reference.find('collab'):
    col=reference.find('collab').get_text()
    collab.append(col)
    return collab
  
def pub_name (reference):
  pub_names=[]
  if reference.find('publisher-name'):
    p_name=reference.find('publisher-name').get_text()
    pub_names.append(p_name)
    return pub_names
  

def pub_location (reference):
  pub_loc=[]
  if reference.find('publisher-loc'):
    p_loc=reference.find('publisher-loc').get_text()
    pub_loc.append(p_loc)
    return pub_loc
 

def ref_year(reference):
  year=[]
  if reference.find('year'):
    yr=reference.find('year').get_text()
    year.append(yr)
    return year
  
def ref_type(reference):
  if reference['publication-type'] :           
    types=reference['publication-type']
    return types

def ref_edition(reference):
  edi=[]
  if reference.find('edition'):
    ed=reference.find('edition').get_text()
    edi.append(ed)
    return edi
  
  
def ref_doi(reference):
  pub_doi=[]
  if reference.find('pub-id-type'):
    doi=reference.find('pub-id-type').get_text()
    pub_doi.append(doi)
    return pub_doi


reference_tags=soup.find_all('element-citation')

references=[]

for reference_tag in reference_tags:
  reference={}
  
  collab = ref_collab(reference_tag)
  if collab:
    reference['author']=collab


  author = person_group(reference_tag)
  if author:
    reference['author']=author


  title = ref_title(reference_tag)
  if title:
    reference['title']=title


  pub_names = pub_name(reference_tag)
  if pub_names:
    reference['publisher']=pub_names

  
  pub_loc = pub_location(reference_tag)
  if pub_loc:
     reference['location']=pub_loc

  
  year = ref_year(reference_tag)
  if year:
    reference['date']=year


  edi=ref_edition(reference_tag)
  if edi:
    reference['edition']=edi

  
  pub_doi = ref_doi(reference_tag)
  if pub_doi:
    reference['doi']=pub_doi


  reference['type']=ref_type(reference_tag)
  references.append(reference)

  
final_out=json.dumps(references,indent=6)
print(final_out)


with open('book.json','w') as outfile:
  outfile.write(final_out)


