const Joi = require('joi');
const express = require("express");
const app = express();
app.use(express.json());
const tasks = [
    { no: 1, task: "Task1" },
    { no: 2, task: "Task2" },
    { no: 3, task: "Task3" },
    { no: 4, task: "Task4" },
    { no: 5, task: "Task5" }
];
app.get('/', (req, res) => {
    res.send(" TO DO API ");

})
app.get('/tasks', (req, res) => {
    res.send(tasks);

})

app.delete('/tasks/:no', (req, res) => {
    const task = tasks.find(c => c.no == parseInt(req.params.no));
    if (!task) res.status(404).send("SELECTED TASK IS NOT FOUND...");
    const index = tasks.indexOf(task);
    tasks.splice(index, 1);
    res.send(task);

});

app.put('/tasks/:no', (req, res) => {
    const task = tasks.find(c => c.no == parseInt(req.params.no));
    if (!task) res.status(404).send("SELECTED TASK IS NOT FOUND...");

    const { error } = validatetask(req.body);
    if (error) {

        res.status(400).send(result.error.details[0].message)
        return;

    }
    task.task = req.body.task;
    res.send(task);

})

app.post('/tasks', (req, res) => {
    const { error } = validatetask(req.body);
    if (error) {

        res.status(400).send(result.error.details[0].message)
        return;

    }

    const task = {
        no: tasks.length + 1,
        task: req.body.task
    };
    tasks.push(task);
    res.send(course);
})


//api/tasks/1
app.get('/tasks/:no', (req, res) => {
    const task = tasks.find(c => c.no == parseInt(req.params.no));
    if (!task) res.status(404).send("SELECTED TASK IS NOT FOUND...");
    res.send(task)


});

function validatetask(task) {
    const schema = {
        task: Joi.string().min(3).required()

    }
    return Joi.validate(task, schema);
}

const port = process.env.PORT || 8000;
app.listen(port, () => console.log("LISTENING TO PORT 8000"));