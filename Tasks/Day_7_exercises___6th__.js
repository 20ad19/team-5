const fs = require('fs');
const xpath = require('xpath-html');

const html = fs.readFileSync(`${__dirname}/sample.html`, 'utf8');

const node5 = xpath.fromPageSource(html).findElements("//span[@corresp='yes']/span[position()<3]");
console.log(node5.toString());
if (node5 !== null) {
    console.log("Up to two corresponding authors with corres=yes ");
} else {
    console.log(" ");
}