var libxml = require('libxmljs');
var parser = new libxml.SaxParser();

var xml = '<?xml version="1.0" encoding="UTF-8"?>' +
    '<bookstore>' +

    ' <book category="cooking">' +
    '<title lang="en">Everyday Italian</title>' +
    '<author>Giada De Laurentiis</author>' +
    ' <year>2005</year>' +
    '<price>30.00</price>' +
    ' </book>' +

    '<book category="children">' +
    '<title lang="en">Harry Potter</title>' +
    ' <author>J K. Rowling</author>' +
    '<year>2005</year>' +
    ' <price>29.99</price>' +
    '</book>' +

    '</bookstore>'

var xmlDoc = libxml.parseXmlString(xml);
var aut = xmlDoc.get("/bookstore//book");
console.log(aut.text());
//txt = xmlDoc.getElementsByTagName("title")[0].childNodes[0].nodeValue;
//console.log(txt.text());